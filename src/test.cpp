//
// Created by dvl on 7/3/19.
//
#include <iostream>

#include "retry_wrapper_async.hpp"
#include "retry_strats/counted.hpp"
#include "async_op_wrapper/recreate.hpp"
#include "async_op_wrapper/keep.hpp"

class example_async_op : std::enable_shared_from_this<example_async_op> {
public:
private:
    int _a;
    int _b;

    int _iteration_target;
    int _iteration_value;

    std::function<void(int, int)> _callback;

public:
    example_async_op(int a, int b)
        : _a{ a }
        , _b{ b }
        , _iteration_target{ 0 }
        , _iteration_value{ 0 }
        , _callback{ nullptr }
    {
    }

    void async_op(std::function<void(int, int)> callback, int value)
    {
        _callback = callback;
        _callback(_a, _b);
    }
};

class example_interceptor : public interceptor_base {
public:
    void on_result(int a, int b)
    {
        notify(a < b);
    }
};

int main()
{
    auto wrapper = async_op_wrapper_base{ &example_async_op::async_op };

    auto obj = retry_wrapper_async{ wrapper, retry_strat_counted{ 50 }, example_interceptor{} };

    obj.bind_creation_arguments(12, 25);
    obj.bind_exec_arguments(&example_async_op::async_op, 500);

    obj.start([](bool result) {
        std::cout << "async_op finished" << (result ? "un" : "") << "successfully" << std::endl;
    });
}
