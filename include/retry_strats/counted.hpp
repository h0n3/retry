//
// Created by dvl on 6/5/19.
//

#ifndef RETRY_COUNTED_HPP
#define RETRY_COUNTED_HPP

#include <boost/asio.hpp>

struct retry_strat_counted {
    retry_strat_counted(std::size_t max_retrys)
        : _max_retrys{ max_retrys }
        , _retry_count{ 0 }
    {
    }

    void did_try()
    {
        ++_retry_count;
    }

    bool has_try()
    {
        return _retry_count < _max_retrys;
    }

    const std::size_t _max_retrys;
    std::size_t       _retry_count;
};

struct retry_strat_counted_delay {
    retry_strat_counted_delay(
        std::size_t max_retrys, boost::asio::io_context& io_context, std::chrono::milliseconds delay)
        : _max_retrys{ max_retrys }
        , _retry_count{ 0 }
        , _timer{ io_context }
        , _delay{ delay }
    {
    }

    void did_try()
    {
        ++_retry_count;

        _timer.expires_after(_delay);
        _timer.wait();
    }

    bool has_try()
    {
        return _retry_count < _max_retrys;
    }

    const std::size_t _max_retrys;
    std::size_t       _retry_count;

    boost::asio::steady_timer _timer;
    std::chrono::milliseconds _delay;
};

#endif // RETRY_COUNTED_HPP
