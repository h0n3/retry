//
// Created by dvl on 6/5/19.
//

#ifndef RETRY_WRAP_HPP
#define RETRY_WRAP_HPP

template <class functor_t>
struct call_strat_wrap {
    bool call(functor_t functor)
    {
        try {
            return functor();
        } catch (...) {
            return false;
        }
    }
};

#endif // RETRY_WRAP_HPP
