//
// Created by dvl on 6/5/19.
//

#ifndef RETRY_COMPARE_HPP
#define RETRY_COMPARE_HPP

template <class functor_t, class compare_t>
struct call_strat_compare {
    compare_t _compare;
    call_strat_compare(compare_t compare)
        : _compare{ compare }
    {
    }
    // i think there is a way to deduce the compare type from the return type of
    // the () operator
    // but then it would not work for custom comparison operators
    bool operator()(functor_t functor)
    {
        try {
            auto func_result = functor();

            if (func_result == _compare)
                return true;
        } catch (...) {
            return false;
        }
    }
};

template <class functor_t, class compare_t, class compare_func>
struct call_strat_compare {
    compare_t _compare;
    call_strat_compare(compare_t compare)
        : _compare{ compare }
    {
    }
    // i think there is a way to deduce the compare type from the return type of
    // the () operator
    // but then it would not work for custom comparison operators
    bool operator()(functor_t functor)
    {
        try {
            auto func_result = functor();

            if (compare_func(func_result, _compare))
                return true;
        } catch (...) {
            return false;
        }
    }
};

#endif // RETRY_COMPARE_HPP
