//
// Created by dvl on 6/5/19.
//

#ifndef RETRY_EXCEPTION_HPP
#define RETRY_EXCEPTION_HPP

template <class functor_t>
struct call_strat_exception_any {
    bool call(functor_t functor)
    {
        try {
            functor();
            return true;
        } catch (...) {
            return false;
        }
    }
};

template <class functor_t, class exception_t>
struct call_strat_exception_specific {
    bool call(functor_t functor)
    {
        try {
            functor();
            return true;
        } catch (const exception_t& exception) {
            return false;
        }
    }
};

#endif // RETRY_EXCEPTION_HPP
