//
// Created by dvl on 7/5/19.
//

#ifndef RETRY_KEEP_HPP
#define RETRY_KEEP_HPP

#include "base.hpp"

template <class async_op_t, class async_op_member_func_t>
class async_op_wrapper_keep : public async_op_wrapper_base<async_op_t, async_op_member_func_t> {
public:
    async_op_wrapper_keep()
    {
        async_op_wrapper_base<async_op_t, async_op_member_func_t>::_async_op_creation_func
            = [this] { return this->_async_op; };
    }

    void set_object(async_op_t* async_op)
    {
        _async_op = async_op;
    }
    template <class... args_t>
    void emplace_object(args_t... args)
    {
        _async_op = new async_op_t{ args... };
    }

private:
    std::shared_ptr<async_op_t> _async_op;
};

#endif // RETRY_KEEP_HPP
