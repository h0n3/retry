//
// Created by dvl on 7/5/19.
//

#ifndef RETRY_RECREATE_HPP
#define RETRY_RECREATE_HPP

#include "base.hpp"

template <class async_op_t, class return_t, class... args_t>
class async_op_wrapper_recreate : public async_op_wrapper_base<async_op_t, return_t, args_t...> {
public:
    async_op_wrapper_recreate(
        typename async_op_wrapper_base<async_op_t, return_t, args_t...>::async_op_member_func_t func)
        : async_op_wrapper_base<async_op_t, return_t, args_t...>{ func }
    {
        async_op_wrapper_recreate<async_op_t, return_t, args_t...>::_async_op_creation_func
            = []() { return std::make_shared<async_op_t>(); };
    }

    template <class... bind_args_t>
    void bind_creation_arguments(bind_args_t... args)
    {
        async_op_wrapper_recreate<async_op_t, return_t, args_t...>::_async_op_creation_func
            = [args...] { return std::make_shared<async_op_t>(args...); };
    }
};

#endif // RETRY_RECREATE_HPP
