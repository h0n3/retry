//
// Created by dvl on 7/5/19.
//

#ifndef RETRY_BASE_HPP
#define RETRY_BASE_HPP

#include <memory>
#include <functional>

template <class F>
struct ArgType;

template <typename Ret, typename Cls, typename T, typename... Args>
struct ArgType<Ret (Cls::*)(T, Args...)> {
    using type = T;
};

template <class async_op_t, class return_t, class... args_t>
class async_op_wrapper_base {
public:
    async_op_wrapper_base(return_t (async_op_t::*function)(args_t...))
        : _mem_func{ function }
    {
    }
    // or a lambda type because we need to be able to store the shared_ptr owning the object
    using async_op_owning_ptr_t = typename std::shared_ptr<async_op_t>;

    using async_op_member_func_t = return_t (async_op_t::*)(args_t...);
    using async_op_callback_t    = typename ArgType<async_op_member_func_t>::type;

    using async_op_creation_func_t = std::function<async_op_owning_ptr_t(void)>;
    using async_op_exec_func_t     = std::function<void(async_op_owning_ptr_t, async_op_callback_t)>;

    template <class... exec_args_t>
    void bind_exec_arguments(exec_args_t... args)
    {
        async_op_wrapper_base<async_op_t, async_op_member_func_t>::_async_op_exec_func
            = [this, args...](typename async_op_wrapper_base<async_op_t,
                                  async_op_member_func_t>::async_op_owning_ptr_t async_op_owning_ptr,
                  typename async_op_wrapper_base<async_op_t, async_op_member_func_t>::async_op_callback_t
                      async_op_callback) {
                  std::invoke(this->_mem_func, async_op_owning_ptr.get(), async_op_callback, args...);
              };
    }

protected:
    async_op_member_func_t _mem_func;

    async_op_creation_func_t _async_op_creation_func;
    async_op_exec_func_t     _async_op_exec_func;
};

#endif // RETRY_BASE_HPP
