//
// Created by dvl on 6/5/19.
//

#ifndef RETRY_RETRY_WRAPPER_ASYNC_HPP
#define RETRY_RETRY_WRAPPER_ASYNC_HPP

#include <boost/asio.hpp>
#include <boost/chrono.hpp>

class interceptor_base {
public:
    using callback_t = std::function<void(bool)>;

    void set_callback(callback_t callback)
    {
        _callback = callback;
    }

    void notify(bool result)
    {
        _callback(result);
    }

private:
    callback_t _callback;
};

template <class class_t, class return_t, class... params_t>
auto bind_member_func_full(return_t (class_t::*member_func)(params_t...), class_t* instance)
{
    return [instance, member_func](
               params_t... args) -> return_t { return (instance.*member_func)(args...); };
}

template <class class_t, class return_t, class... params_t>
auto bind_member_func_full(return_t (class_t::*member_func)(params_t...), class_t& instance)
{
    return [&instance, member_func](
               params_t... args) -> return_t { return (instance.*member_func)(args...); };
}

template <class retry_strat_t, class interceptor_t, class async_op_wrapper_t>
class retry_wrapper_async : public async_op_wrapper_t,
                            public std::enable_shared_from_this<
                                retry_wrapper_async<retry_strat_t, interceptor_t, async_op_wrapper_t>> {
public:
    using this_t = retry_wrapper_async<retry_strat_t, interceptor_t, async_op_wrapper_t>;

private:
    retry_strat_t _retry_strat;

    typename async_op_wrapper_t::async_op_owning_ptr_t _active_async_op;
    std::function<void(bool)>                          _result_callback;

    // FXIME remove this when adding the proper lifetime managment
    std::function<std::shared_ptr<interceptor_t>(void)> _create_interceptor_func;

public:
    retry_wrapper_async(
        retry_strat_t retry_strat, async_op_wrapper_t async_op_wrapper, interceptor_t interceptor)
        : async_op_wrapper_t{ async_op_wrapper }
        , _retry_strat{ retry_strat }
        , _active_async_op{ nullptr }
        , _result_callback{ nullptr }
    {
        _create_interceptor_func = std::make_shared<interceptor_t>();
    }

    template <class... args_t>
    void bind_create_interceptor_arguments(args_t... args)
    {
        _create_interceptor_func = [args...] { return std::make_shared<interceptor_t>(args...); };
    }

    std::shared_ptr<interceptor_t> setup_interceptor()
    {
        std::shared_ptr<interceptor_t> ptr;
        ptr = _create_interceptor_func();
        auto on_try_complete_func
            = std::bind(&this_t::on_try_complete, this_t::shared_from_this(), std::placeholders::_1);
        ptr->set_callback(on_try_complete_func);

        return ptr;
    }

    void start(std::function<void(bool)> result_callback)
    {
        _result_callback = result_callback;

        async_try();
    }

    void async_try()
    {
        auto async_op = async_op_wrapper_t::_async_op_creation_func();

        auto interceptor = setup_interceptor();
        auto call_back   = bind_member_func_full(&interceptor_t::on_result, interceptor);

        async_op_wrapper_t::_async_op_exec_func(async_op, call_back);
    }

    void on_try_complete(bool result)
    {
        _retry_strat.did_try();

        if (result) {
            _result_callback(true);
        } else {
            if (_retry_strat.has_try()) {
                async_try();
            } else {
                _result_callback(false);
            }
        }
    }
};

#endif // RETRY_RETRY_WRAPPER_ASYNC_HPP
