//
// Created by dvl on 6/5/19.
//

#ifndef RETRY_RETRY_WRAPPER_SYNC_HPP
#define RETRY_RETRY_WRAPPER_SYNC_HPP

#include <boost/asio.hpp>
#include <boost/chrono.hpp>

template <class functor_t, class retry_strat_t, class call_strat_t>
class retry_wrapper_sync : private retry_strat_t, private call_strat_t {
private:
    functor_t _functor;

public:
    retry_wrapper_sync(functor_t functor, retry_strat_t retry_strat, call_strat_t call_strat)
        : retry_strat_t{ retry_strat }
        , call_strat_t{ call_strat }
        , _functor{ functor }
    {
    }

    template <class... arg_list_t>
    bool operator()(arg_list_t)
    {
        while (has_try()) {

            bool result;
            try {
                result = ::call_strat_t(_functor);
            } catch (...) {
                return false;
            }
            did_try();

            if (result) {
                return true;
            }
        }
        return false;
    }
};

/*
template <class functor_t>
using sync_retryer_delayed =
    retry_wrapper_sync<functor_t, retry_strat_counted_delay,
                       call_strat_any_exception<functor_t>>;
*/

#endif // RETRY_RETRY_WRAPPER_SYNC_HPP
