cmake_minimum_required(VERSION 3.14)

set(CMAKE_CXX_STANDARD 17)

include_directories(include)

add_executable(test src/test.cpp)

target_link_libraries(test pthread boost_program_options
        boost_thread boost_coroutine
        ssl crypto)

